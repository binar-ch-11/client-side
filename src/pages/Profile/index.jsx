import React, { useEffect, useState } from 'react';
import Cookies from 'js-cookie';
import axios from 'axios';
import { io } from 'socket.io-client';
import { useNavigate } from 'react-router-dom';
import Navbar from '../../components/Navbar';
import Loading from '../../components/Loading';

const socket = io(process.env.REACT_APP_API);

function Profile() {
  const [showModal, setShowModal] = useState(false);
  const [token, setToken] = useState(null);
  const [user, setUser] = useState(null);
  const [editing, setEditing] = useState(false);
  const [image, setImage] = useState('');
  const [isLoading, setIsLoading] = useState(false);

  const navigate = useNavigate();

  useEffect(() => {
    if (Cookies.get('token')) setToken(Cookies.get('token'));
  }, []);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axios.get(`${process.env.REACT_APP_API}/user`, {
          headers: { Authorization: `${Cookies.get('token')}` },
        });
        setUser(response.data.data);
      } catch (error) {
        alert('error');
      }
    };

    fetchData();
  }, [token]);

  const handlerLogout = async () => {
    setIsLoading(true);
    try {
      await axios.get(`${process.env.REACT_APP_API}/logout`, {
        headers: { Authorization: `${Cookies.get('token')}` },
      });
      Cookies.remove('token');
      Cookies.remove('id');
      Cookies.remove('name');
      socket.emit('login_logout', 'Logout');
      setIsLoading(false);
      navigate('/');
    } catch (error) {
      alert(error);
      setIsLoading(false);
    }
  };

  function handleChange(e) {
    setImage(e.target.files[0]);
  }

  const uploadImage = async () => {
    setIsLoading(true);
    const data = new FormData();
    data.append('file', image);
    data.append('upload_preset', 'profile');
    data.append('cloud_name', 'dyx8tahem');
    try {
      const cloudRespone = await axios.post('https://api.cloudinary.com/v1_1/dyx8tahem/image/upload', data);
      const response = await axios.post(`${process.env.REACT_APP_API}/update-profile`, { secure_url: cloudRespone.data.secure_url }, { headers: { Authorization: `${Cookies.get('token')}` } });
      setUser(response.data.data);
      setEditing(false);
      setIsLoading(false);
    } catch (error) {
      alert(error);
      setIsLoading(false);
    }
  };

  return (
    <div className="min-h-screen bg-white flex justify-center">
      <Navbar />
      {isLoading ? (
        <Loading />
      ) : null}
      <div className="flex flex-col flex-none w-[75vw] md:w-[500px]">
        <button
          type="button"
          className="self-center mt-[70px] hover:cursor-zoom-in"
          onClick={() => setShowModal(true)}
        >
          <img
            src={`${user?.image}`}
            className="w-[200px] h-[200px] rounded-full items-center"
            alt="profile"
          />
        </button>
        {showModal ? (
          <button
            type="button"
            className="absolute top-0 left-0 bottom-0 right-0 z-40"
            style={{ backgroundColor: 'rgba(0,0,0,0.5)' }}
            onClick={() => setShowModal(false)}
          >
            <div className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none">
              <div className="relative w-auto my-6 mx-auto max-w-3xl">
                {/* content */}
                <div className="relative flex flex-col w-full focus:outline-none">
                  {/* body */}
                  <div className="relative p-3 flex-auto">
                    <img
                      src={`${user?.image}`}
                      className="w-[100vh] items-center"
                      alt="profile"
                    />
                  </div>
                </div>
              </div>
            </div>
            {/* <div></div> */}
          </button>
        ) : null}
        <p className="mt-4 text-justify">
          &nbsp;&nbsp;&nbsp;&nbsp;Perkenalkan nama Saya
          {' '}
          {user?.name}
          . Saya
          merupakan Student Binary Wave ke 24. Selama di Binary Saya mendapatkan
          wawasan baru mengenai Full Stack Web Development, dapat teman baru
          dari seluruh Indonesia.
        </p>
        <p className="mt-2 text-justify underline">Kesan :</p>
        <p className="mt-2 text-justify">
          &nbsp;&nbsp;&nbsp;&nbsp;Lelah, Pusing, Ngantuk. Walapun begitu Saya
          mendapakan ilmu yang berharga di Academy Binary ini. Serta
          {' '}
          <br />
          Saya mengucapkan banyak terimakasih kepada Mas Hasan yang telah
          membimbing saya selama ini dari Chapter 3 sampai Chapter 11. Tidak
          lupa kepada rekan rekan team 1 yang telah banyak membantu dan tetap
          solid untuk menyelesaikan setiap challange di chapter 9 sampai 11.
        </p>
        <div className="flex gap-3 self-center">
          <button
            type="button"
            className=" bg-cyan-400 py-3 px-4 rounded-lg mt-20 w-52"
            onClick={() => setEditing(true)}
          >
            Edit Foto Profile
          </button>
          <button
            type="button"
            className="bg-red-500 py-3 px-4 rounded-lg mt-20 w-52"
            onClick={handlerLogout}
          >
            Logout
          </button>
        </div>
        {editing ? (
          <form className="flex self-center mt-10">
            <input type="file" onChange={handleChange} />
            <button type="button" className="py-2 px-3 bg-green-500 text-white rounded-lg" onClick={uploadImage}>Upload</button>
          </form>
        ) : null}
      </div>
    </div>
  );
}

export default Profile;
