import React from 'react';
import {
  render,
  screen,
  fireEvent,
  waitFor,
} from '@testing-library/react';
import '@testing-library/jest-dom';
import Login from './index';

jest.mock('axios');
jest.mock('js-cookie');
jest.mock('socket.io-client');
jest.mock('react-router-dom');

describe('Login', () => {
  it('Should be Login', () => {
    render(<Login />);

    expect(screen.getByPlaceholderText('email')).toBeInTheDocument();
    expect(screen.getByPlaceholderText('password')).toBeInTheDocument();
    expect(screen.getByText('Login')).toBeInTheDocument();
  });

  it('Should error handling', async () => {
    render(<Login />);
    const buttonLogin = screen.getByText('Login');

    expect(screen.queryByText('Email/Password incorrect!')).toBeNull();

    fireEvent.click(buttonLogin);
    await waitFor(() => {
      expect(screen.getByText('Email/Password incorrect!')).toBeInTheDocument();
    });
  });
});
