import React, { useState } from 'react';
import axios from 'axios';
import Cookies from 'js-cookie';
import { io } from 'socket.io-client';
import {
  HiOutlineEnvelopeOpen,
  HiOutlineLockClosed,
  HiUser,
} from 'react-icons/hi2';
import { Link, useNavigate } from 'react-router-dom';
import Loading from '../../components/Loading';
import Background from '../../assets/img/bg-1.jpg';

const socket = io(process.env.REACT_APP_API);

function Login() {
  const [input, setInput] = useState({
    email: '',
    password: '',
  });
  const [isLoading, setIsLoading] = useState(false);
  const [textHeader, setTextHeader] = useState('Login Form');

  const navigate = useNavigate();

  const handlerChange = (e) => {
    setInput({ ...input, [e.target.name]: e.target.value });
  };

  const handlerSubmit = async (e) => {
    e.preventDefault();
    setIsLoading(true);
    try {
      const response = await axios.post(
        `${process.env.REACT_APP_API}/login`,
        input,
      );
      Cookies.set('token', response.data.accessToken);
      Cookies.set('id', response.data.id);
      Cookies.set('name', response.data.name);
      socket.emit('login_logout', 'Login');
      navigate('/profile');
      setIsLoading(false);
    } catch (error) {
      setTextHeader('Email/Password incorrect!');
      setIsLoading(false);
    }
  };

  return (
    <div
      style={{ backgroundImage: `url(${Background})` }}
      className="flex justify-center min-h-screen bg-cover"
    >
      {isLoading ? <Loading /> : null}
      <div
        style={{
          boxShadow: '0px 0px 25px 10px black',
          background: 'rgba(4, 29, 23, 0.5)',
        }}
        className="self-center py-3 px-8 rounded-lg flex flex-col gap-5 lg:w-1/4 w-3/4"
      >
        <div className="flex justify-center -mt-8">
          <div
            style={{ boxShadow: '0px 0px 6px 5px black' }}
            className="w-max p-3 rounded-full bg-black text-white"
          >
            <HiUser />
          </div>
        </div>
        <h2 className="text-2xl text-center font-bold mb-6">{textHeader}</h2>
        <form
          className="flex flex-col gap-6"
          action=""
          method="post"
          onSubmit={handlerSubmit}
        >
          <div className="flex gap-3 border-b-2 border-slate-700 pb-1">
            <span className="self-center">
              <HiOutlineEnvelopeOpen />
            </span>
            <input
              className="bg-transparent"
              name="email"
              onChange={handlerChange}
              type="email"
              placeholder="email"
              value={input.name}
            />
          </div>
          <div className="flex gap-3 border-b-2 border-slate-700 pb-1">
            <span className="self-center">
              <HiOutlineLockClosed />
            </span>
            <input
              className="bg-transparent"
              name="password"
              onChange={handlerChange}
              type="password"
              placeholder="password"
              value={input.password}
            />
          </div>
          <button
            className="border border-slate-700 py-2 font-semibold hover:shadow-2xl hover:shadow-black hover:text-white"
            type="submit"
          >
            Login
          </button>
        </form>
        <Link className="hover:text-white" to="/register">
          Register
        </Link>
      </div>
    </div>
  );
}

export default Login;
