import React, { useEffect, useRef, useState } from 'react';
import { io } from 'socket.io-client';
import axios from 'axios';
import Cookies from 'js-cookie';
import { saveAs } from 'file-saver';
import {
  HiOutlinePhoto,
  HiOutlineXMark,
  HiPaperAirplane,
  HiArrowDownTray,
  HiMicrophone,
  HiDocument,
} from 'react-icons/hi2';
import { ProgressBar } from 'react-loader-spinner';
import { Player } from 'video-react';
import moment from 'moment';
import momentTz from 'moment-timezone';
import Navbar from '../../components/Navbar';

const socket = io(process.env.REACT_APP_API);

function Chat() {
  const [input, setInput] = useState('');
  const [imageMessage, setImageMessage] = useState(null);
  const [messages, setMessages] = useState(null);
  const [users, setUsers] = useState(null);
  const [dynamicButton, setDynamicButton] = useState(false);

  const newMessage = useRef(null);
  const dummyRef = useRef(null);

  useEffect(() => {
    const fetch = async () => {
      try {
        const response = await axios.get(
          `${process.env.REACT_APP_API}/all-message`,
        );
        setMessages(response.data.data);
        const responseUsers = await axios.get(
          `${process.env.REACT_APP_API}/all-user`,
        );
        setUsers(responseUsers.data.data);
      } catch (error) {
        alert(error);
      }
    };

    fetch();
  }, []);

  useEffect(() => {
    socket.on('receive_message', (data) => {
      setMessages(data);
      newMessage.current.scrollIntoView({ block: 'start' });
    });

    socket.on('status_user', (statusUsers) => {
      setUsers(statusUsers);
    });
  }, [socket, messages, users]);

  const getFormData = (object) => {
    const formData = new FormData();
    Object.keys(object).forEach((key) => formData.append(key, object[key]));
    return formData;
  };

  const sendMessage = async (e) => {
    e.preventDefault();
    setDynamicButton(true);
    try {
      const payloadObj = {
        message: input,
        image_message: imageMessage,
        user_id: Number(Cookies.get('id')),
      };
      const payload = getFormData(payloadObj);
      await axios.post(`${process.env.REACT_APP_API}/send-message`, payload);
      socket.emit('send_message', 'test');
      setInput('');
      setImageMessage(null);
      setDynamicButton(false);
    } catch (error) {
      alert(error);
      setDynamicButton(false);
    }
  };

  return (
    <div className="max-h-screen p-10 flex background-chat ">
      <Navbar />
      <div className="rounded-lg border border-green-500 w-full p-7 flex flex-col gap-8">
        <div className="flex flex-col gap-5 h-[650px] overflow-auto">
          {messages ? (
            <>
              {messages.map((item, index) => {
                let userName = '';
                let photoProfile = '';
                let isOnline = false;
                let statusOffline = '';
                let selfStyle = false;
                users?.forEach((user) => {
                  if (user.id === item.user_id) {
                    userName = user.name;
                    photoProfile = user.image;

                    if (user.is_online) isOnline = true;
                    else statusOffline = momentTz(user.updatedAt).tz('Asia/Jakarta').format();
                  }
                });

                if (item.user_id === Number(Cookies.get('id'))) {
                  selfStyle = true;
                }
                return (
                  <div
                    key={item.id}
                    ref={index === messages.length - 1 ? newMessage : dummyRef}
                    className={`p-5 ${
                      selfStyle
                        ? 'border-green-500 self-end'
                        : 'border-blue-500 self-start'
                    } pt-2 border bg-slate-800 rounded-lg max-w-max w-1/2 min-w-min`}
                  >
                    <div
                      className={`flex ${
                        selfStyle
                          ? 'justify-end'
                          : 'justify-end flex-row-reverse'
                      } gap-3 bg-slate-700 p-2 rounded-lg mb-1`}
                    >
                      {isOnline ? (
                        <p className="text-green-500">online</p>
                      ) : (
                        <p className="text-red-500 text-xs">
                          Active
                          {' '}
                          {moment(statusOffline, 'YYYY-MM-DD h:mm:s').fromNow()}
                        </p>
                      )}
                      <p className="self-center text-slate-200">{userName}</p>
                      <figure className="figure w-6 h-6 self-center rounded-full overflow-hidden">
                        {photoProfile ? (
                          <img
                            className="w-6 h-6"
                            src={`${photoProfile}`}
                            alt="profile"
                          />
                        ) : null}
                      </figure>
                    </div>
                    {item.media ? (
                      <div
                        className={` ${
                          selfStyle ? 'flex justify-end' : ''
                        } relative`}
                      >
                        {item.is_file ? (
                          <>
                            <Player
                              className=""
                              fluid={false}
                              width={700}
                              src={`${item.media}`}
                              alt="image_message"
                            />
                            <button
                              type="button"
                              onClick={() => saveAs(item.media, 'video.mp4')}
                              className="absolute top-3 bg-slate-500 p-2 rounded-full right-3 text-white"
                            >
                              <HiArrowDownTray />
                            </button>
                          </>
                        ) : (
                          <>
                            <img
                              className="w-full"
                              src={`${item.media}`}
                              alt="image_message"
                            />
                            <button
                              type="button"
                              onClick={() => saveAs(item.media, 'image.jpg')}
                              className="absolute top-3 bg-slate-500 p-2 rounded-full right-3 text-white"
                            >
                              <HiArrowDownTray />
                            </button>
                          </>
                        )}
                      </div>
                    ) : null}
                    <p
                      className={`text-slate-300 ${
                        selfStyle ? 'flex justify-end mr-2' : ''
                      }`}
                    >
                      {item?.message}
                    </p>
                  </div>
                );
              })}
            </>
          ) : null}
        </div>
        <form
          onSubmit={sendMessage}
          method="POST"
          className="bg-slate-500 rounded-l py-4 flex justify-center gap-9 px-2"
        >
          <div className="flex row gap-3">
            <label>
              <HiDocument className="w-9 h-9 text-white hover:cursor-not-allowed" />
            </label>
            <div className="relative overflow-hidden flex">
              <input
                id="image-message"
                className="hidden"
                type="file"
                name="image_message"
                onChange={(e) => setImageMessage(e.target.files[0])}
              />
              {!imageMessage ? (
                <label htmlFor="image-message">
                  <HiOutlinePhoto className="w-10 h-10 text-white hover:cursor-pointer" />
                </label>
              ) : null}
              {imageMessage ? (
                <div className="flex gap-3 ">
                  <p className="text-white italic self-center">
                    {imageMessage.name}
                  </p>
                  {' '}
                  <button
                    type="button"
                    onClick={() => setImageMessage(null)}
                    className="bg-slate-600 p-1 rounded-full self-center hover:bg-slate-700 hover:cursor-pointer"
                  >
                    <HiOutlineXMark />
                  </button>
                </div>
              ) : null}
            </div>
          </div>
          <input
            type="text"
            value={input}
            placeholder="Text..."
            onChange={(e) => setInput(e.target.value)}
            className="w-3/5 rounded px-2"
          />
          {input === '' && !imageMessage ? (
            <div className="flex">
              <HiMicrophone className="w-8 h-8 self-center text-white hover:cursor-not-allowed" />
            </div>
          ) : (
            // eslint-disable-next-line react/jsx-no-useless-fragment
            <>
              {dynamicButton ? (
                <span className="self-center">
                  <ProgressBar
                    height="40"
                    width="40"
                    ariaLabel="progress-bar-loading"
                    wrapperStyle={{}}
                    wrapperClass="progress-bar-wrapper"
                    borderColor="#F4442E"
                    barColor="#51E5FF"
                  />
                </span>
              ) : (
                <button type="submit">
                  <HiPaperAirplane className="w-10 h-10 text-white hover:cursor-pointer" />
                </button>
              )}
            </>
          )}
        </form>
      </div>
    </div>
  );
}

export default Chat;
