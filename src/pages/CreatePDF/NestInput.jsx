import React, { useState } from 'react';
import { HiOutlineTrash, HiOutlinePlus } from 'react-icons/hi2';

function NestInput({
  firstInputPlaceholder, secondInputPlaceholder, firstKeyInput, secondKeyInput, input, onChange,
}) {
  const [inputLength, setInputLength] = useState(1);
  const result = [];

  for (let i = 1; i <= inputLength; i += 1) {
    const temp = (
      <div>
        <div className="flex justify-evenly">
          <input type="text" name={`${firstKeyInput}${i}`} value={input[`${firstKeyInput}${i}`] ? input[`${firstKeyInput}${i}`] : ''} placeholder={firstInputPlaceholder} onChange={onChange} className="bg-slate-200 rounded-lg py-1 px-2 text-black mb-1" />
          <input type="text" name={`${secondKeyInput}${i}`} value={input[`${secondKeyInput}${i}`] ? input[`${secondKeyInput}${i}`] : ''} placeholder={secondInputPlaceholder} onChange={onChange} className="bg-slate-200 rounded-lg py-1 px-2 text-black mb-1" />
        </div>
        <div className="flex justify-evenly gap-3">
          <div>
            <p className="font-semibold">From :</p>
            <input type="date" placeholder="From" name={`${firstKeyInput}From${i}`} value={input[`${firstKeyInput}From${i}`] ? input[`${firstKeyInput}From${i}`] : ''} onChange={onChange} className="bg-slate-200 rounded-lg py-1 px-2 text-black mb-1" />
          </div>
          <div>
            <p className="font-semibold">To :</p>
            <input type="date" placeholder="To" name={`${firstKeyInput}To${i}`} value={input[`${firstKeyInput}To${i}`] ? input[`${firstKeyInput}To${i}`] : ''} onChange={onChange} className="bg-slate-200 rounded-lg py-1 px-2 text-black mb-1" />
          </div>
        </div>
        <div className="flex justify-center gap-4">
          {i > 1 && i === inputLength ? (
            <button
              onClick={() => {
                setInputLength(inputLength - 1);
                input[`${firstKeyInput}${i}`] = '';
                input[`${secondKeyInput}${i}`] = '';
                input[`${firstKeyInput}From${i}`] = '';
                input[`${firstKeyInput}To${i}`] = '';
              }}
              className="bg-slate-700 p-2 rounded-full text-white"
              type="button"
            >
              <HiOutlineTrash />
            </button>
          ) : null}
          {i < 3 && i === inputLength ? (
            <button onClick={() => setInputLength(inputLength + 1)} className="border border-slate-700 p-2 rounded-full text-black" type="button">
              <HiOutlinePlus />
            </button>
          ) : null}
        </div>
        <br />
      </div>
    );

    result.push(temp);
  }
  return result;
}

export default NestInput;
