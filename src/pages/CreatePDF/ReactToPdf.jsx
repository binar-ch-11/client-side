import React, { useRef } from 'react';
import { useReactToPrint } from 'react-to-print';
import { HiArrowDownTray } from 'react-icons/hi2';
import moment from 'moment';

function ReactToPdf({ input, setIsAlready, image }) {
  const componentRef = useRef();

  const handlePdf = useReactToPrint({
    content: () => componentRef.current,
    documentTitle: 'test',
  });
  return (
    <>
      <div
        ref={componentRef}
        style={{ width: '100%', height: window.innerHeight }}
        className="bg-white text-black"
      >
        <div className="flex pt-10">
          {/* SISI KIRI */}
          <div className="w-1/4 pl-5 flex flex-col gap-10">
            <div className="flex justify-center">
              {image ? (
                <img className="w-32 h-44 rounded" src={image} alt="pic" />
              ) : null}
            </div>
            <div>
              <h2 className="font-bold text-xl mb-2">Contact Details</h2>
              <div>
                <strong>Phone</strong>
                <p>{input?.phone}</p>
              </div>
              <div>
                <strong>E-mail</strong>
                <p>{input?.email}</p>
              </div>
              <div>
                <strong>Location</strong>
                <p>{input?.location}</p>
              </div>
            </div>
            <div>
              <h2 className="font-bold text-xl mb-2">Skills</h2>
              <p>{input?.skills1}</p>
              <p>{input?.skills2}</p>
              <p>{input?.skills3}</p>
            </div>
            <div>
              <h2 className="font-bold text-xl mb-2">Hobbies</h2>
              <p>{input?.hobby1}</p>
              <p>{input?.hobby2}</p>
              <p>{input?.hobby3}</p>
            </div>
          </div>
          {/* SISI KANAN */}
          <div className="w-3/4 flex flex-col gap-10 pr-9">
            <div className="flex justify-end">
              <h2 className="text-3xl font-bold mb-32">{input?.name}</h2>
            </div>
            <div>
              <h2 className="font-bold text-xl mb-2">About</h2>
              <hr />
              <p>{input?.about}</p>
            </div>
            <div>
              <h2 className="font-bold text-xl mb-2">Education</h2>
              <hr />
              {input?.education1 ? (
                <div className="flex justify-between mb-2">
                  <div>
                    <p className="font-semibold">{input.education1}</p>
                    <p>{input?.degree1}</p>
                  </div>
                  <p>
                    {moment(input?.educationFrom1).format('MMMM YYYY')}
                    {' '}
                    -
                    {' '}
                    {moment(input?.educationTo1).format('MMMM YYYY')}
                  </p>
                </div>
              ) : null}
              {input?.education2 && input?.degree2 && input?.educationFrom2 ? (
                <div className="flex justify-between mb-2">
                  <div>
                    <p className="font-semibold">{input.education2}</p>
                    <p>{input?.degree2}</p>
                  </div>
                  <p>
                    {moment(input?.educationFrom2).format('MMMM YYYY')}
                    {' '}
                    -
                    {' '}
                    {moment(input?.educationTo2).format('MMMM YYYY')}
                  </p>
                </div>
              ) : null}
              {input?.education3 ? (
                <div className="flex justify-between mb-2">
                  <div>
                    <p className="font-semibold">{input.education3}</p>
                    <p>{input?.degree3}</p>
                  </div>
                  <p>
                    {moment(input?.educationFrom3).format('MMMM YYYY')}
                    {' '}
                    -
                    {' '}
                    {moment(input?.educationTo3).format('MMMM YYYY')}
                  </p>
                </div>
              ) : null}
            </div>
            {input?.workExperience1 ? (
              <div>
                <h2 className="font-bold text-xl mb-2">Work Expreiance</h2>
                <hr />
                {input?.workExperience1 && input?.position1 && input?.workExperienceFrom1 ? (
                  <div className="flex justify-between">
                    <div>
                      <strong>{input?.workExperience1}</strong>
                      <p>{input?.position1}</p>
                    </div>
                    <div>
                      <p>
                        {moment(input?.workExperienceFrom1).format('MMMM YYYY')}
                        {' '}
                        -
                        {' '}
                        {moment(input?.workExperienceTo1).format('MMMM YYYY')}
                      </p>
                    </div>
                  </div>
                ) : null}
                {input?.workExperience2 && input?.position2 && input?.workExperienceFrom2 ? (
                  <div className="flex justify-between">
                    <div>
                      <strong>{input?.workExperience2}</strong>
                      <p>{input?.position2}</p>
                    </div>
                    <div>
                      <p>
                        {moment(input?.workExperienceFrom2).format('MMMM YYYY')}
                        {' '}
                        -
                        {' '}
                        {moment(input?.workExperienceTo2).format('MMMM YYYY')}
                      </p>
                    </div>
                  </div>
                ) : null}
                {input?.workExperience3 && input?.position3 && input?.workExperienceFrom3 ? (
                  <div className="flex justify-between">
                    <div>
                      <strong>{input?.workExperience3}</strong>
                      <p>{input?.position3}</p>
                    </div>
                    <div>
                      <p>
                        {moment(input?.workExperienceFrom3).format('MMMM YYYY')}
                        {' '}
                        -
                        {' '}
                        {moment(input?.workExperienceTo3).format('MMMM YYYY')}
                      </p>
                    </div>
                  </div>
                ) : null}
              </div>
            ) : null}
          </div>
        </div>
      </div>
      <div className="absolute top-10 left-1/3 flex gap-5">
        <button
          type="button"
          className="bg-green-400 py-2 px-4 rounded text-black"
          onClick={() => setIsAlready(false)}
        >
          Back
        </button>
        <button
          type="button"
          className="bg-slate-400 py-2 px-4 rounded text-black flex"
          onClick={handlePdf}
        >
          <span>Download</span>
          {' '}
          <span className="self-center">
            <HiArrowDownTray />
          </span>
        </button>
      </div>
    </>
  );
}

export default ReactToPdf;
