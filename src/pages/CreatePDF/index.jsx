import React, { useState } from 'react';
import { HiOutlinePlus } from 'react-icons/hi2';
import { Link } from 'react-router-dom';
import InputsAdd from './InputsAdd';
import NestInput from './NestInput';
import ReactToPdf from './ReactToPdf';

function CreatePDF() {
  const [isAlready, setIsAlready] = useState(false);
  const [inputHobbyLength, setInputHobbyLength] = useState(1);
  const [inputSkillsLength, setInputSkillsLength] = useState(1);
  const [input, setInput] = useState({});
  const [image, setImage] = useState(null);

  const handleChange = (e) => {
    setInput({ ...input, [e.target.name]: e.target.value });
  };

  return (
    !isAlready ? (
      <div className="min-h-screen bg-slate-100 p-3 flex flex-col gap-12">
        <div className="flex justify-around gap-10">
          <div className="bg-white shadow-lg rounded-lg h-52 w-full flex flex-col p-5">
            <label
              className={`${
                image ? 'p-0' : 'p-10'
              } self-center bg-slate-200 rounded-lg border border-dashed border-slate-800 hover:cursor-pointer`}
              htmlFor="image"
            >
              {!image ? (
                '6 X 4'
              ) : (
                <img className="w-20 h-28" src={image} alt="picc" />
              )}
            </label>
            <input
              id="image"
              type="file"
              value={input?.image}
              required
              onChange={(e) => {
                const [file] = e.target.files;
                if (file) {
                  setImage(URL.createObjectURL(file));
                }
              }}
              className="hidden"
            />
            <strong>Name :</strong>
            <input
              className="bg-slate-200 rounded-lg py-2 px-2 text-black"
              type="text"
              name="name"
              required
              value={input?.name}
              onChange={handleChange}
              placeholder="Example"
            />
          </div>
          <div className="bg-white shadow-lg rounded-lg h-52 w-full flex flex-col p-5">
            <strong>Hobby :</strong>
            <InputsAdd
              input={input}
              initial="hobby"
              inputLength={inputHobbyLength}
              handleChange={handleChange}
              setInputLength={setInputHobbyLength}
            />
            {inputHobbyLength !== 3 ? (
              <button
                type="button"
                className="self-center mt-3 border border-slate-700 p-2 rounded-full text-black"
                onClick={() => setInputHobbyLength(inputHobbyLength + 1)}
              >
                <HiOutlinePlus />
              </button>
            ) : null}
          </div>
          <div className="bg-white shadow-lg rounded-lg h-52 w-full flex flex-col p-5">
            <strong>Skills :</strong>
            <InputsAdd
              input={input}
              initial="skills"
              inputLength={inputSkillsLength}
              handleChange={handleChange}
              setInputLength={setInputSkillsLength}
            />
            {inputSkillsLength !== 3 ? (
              <button
                type="button"
                className="self-center mt-3 border border-slate-700 p-2 rounded-full text-black"
                onClick={() => setInputSkillsLength(inputSkillsLength + 1)}
              >
                <HiOutlinePlus />
              </button>
            ) : null}
          </div>
        </div>
        <div className="flex justify-around gap-10">
          <div className="bg-white shadow-lg rounded-lg w-1/2 h-96 flex flex-col pt-3 pl-6">
            <div>
              <strong>Phone :</strong>
              <br />
              <input
                type="tel"
                name="phone"
                required
                value={input?.phone}
                onChange={handleChange}
                className="bg-slate-200 rounded-lg py-1 px-2 text-black mb-1"
              />
            </div>
            <div>
              <strong>E-mail :</strong>
              <br />
              <input
                type="email"
                name="email"
                required
                value={input?.email}
                onChange={handleChange}
                className="bg-slate-200 rounded-lg py-1 px-2 text-black mb-1"
              />
            </div>
            <div>
              <strong>Location :</strong>
              <br />
              <input
                name="location"
                required
                value={input?.location}
                onChange={handleChange}
                type="text"
                className="bg-slate-200 rounded-lg py-1 px-2 text-black mb-1"
              />
            </div>
            <div className="pr-3">
              <strong>About :</strong>
              <br />
              <textarea
                className="bg-slate-200 rounded-lg py-1 px-2 text-black mb-1 w-full"
                name="about"
                id=""
                required
                value={input?.about}
                onChange={handleChange}
                cols="4"
                rows="6"
              />
            </div>
          </div>
          <div className="bg-white shadow-lg rounded-lg h-[420px] w-full p-1 flex justify-between gap-3">
            <div className="w-1/2">
              <strong>Education :</strong>
              <br />
              <NestInput firstInputPlaceholder="School Name" secondInputPlaceholder="Degree" firstKeyInput="education" secondKeyInput="degree" input={input} onChange={handleChange} />
            </div>
            <div className="w-1/2">
              <strong>Work Experience :</strong>
              <br />
              <NestInput firstInputPlaceholder="Company Name" secondInputPlaceholder="Position" firstKeyInput="workExperience" secondKeyInput="position" input={input} onChange={handleChange} />
            </div>
          </div>
        </div>
        <div className="flex justify-evenly">
          <Link
            className="py-2 px-3 font-semibold bg-red-500 rounded-lg text-slate-800 hover:bg-red-600"
            to="/profile"
          >
            Cancel
          </Link>
          <button
            type="button"
            className="py-2 px-3 font-semibold bg-green-500 rounded-lg text-white hover:bg-green-600"
            onClick={() => setIsAlready(true)}
          >
            Generate Resume
          </button>
        </div>
      </div>
    ) : (
      <ReactToPdf input={input} setIsAlready={setIsAlready} image={image} />
    )
  );
}

export default CreatePDF;
