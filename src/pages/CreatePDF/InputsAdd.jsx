import React from 'react';
import { HiOutlineTrash } from 'react-icons/hi2';

const InputsAdd = ({
  inputLength,
  input,
  handleChange,
  setInputLength,
  initial,
}) => {
  const result = [];
  for (let i = 1; i <= inputLength; i += 1) {
    const temp = (
      <div key={i} className="flex gap-5">
        <input
          type="text"
          required={i === 1}
          name={`${initial}${i}`}
          value={input[`${initial}${i}`] ? `${input[`${initial}${i}`]}` : ''}
          onChange={handleChange}
          className="bg-slate-200 rounded-lg py-1 px-2 text-black mb-1"
        />
        {i > 1 && i === inputLength ? (
          <button
            type="button"
            onClick={() => {
              setInputLength(inputLength - 1);
              input[`${initial}${i}`] = '';
            }}
          >
            <HiOutlineTrash />
          </button>
        ) : null}
      </div>
    );

    result.push(temp);
  }

  return result;
};

export default InputsAdd;
