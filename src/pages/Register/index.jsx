import React, { useState } from 'react';
import axios from 'axios';
import { Link, useNavigate } from 'react-router-dom';
import { IoIosArrowBack } from 'react-icons/io';
import Loading from '../../components/Loading';

function Register() {
  const [input, setInput] = useState({
    name: '',
    email: '',
    password: '',
  });
  const [isLoading, setIsLoading] = useState(false);
  const [textHeader, setTextHeader] = useState('Register');

  const navigate = useNavigate();

  const handlerChange = (e) => {
    setInput({ ...input, [e.target.name]: e.target.value });
  };

  const handlerSubmit = async (e) => {
    e.preventDefault();
    setIsLoading(true);
    try {
      await axios.post(`${process.env.REACT_APP_API}/register`, input);
      setIsLoading(false);
      navigate('/');
    } catch (error) {
      if (error.response?.status === 402) {
        setTextHeader('Username already exist!');
      }
      if (error.response?.status === 401) {
        setTextHeader('email already exist!');
      }
      setIsLoading(false);
    }
  };

  return (
    <div className="bg-white h-screen flex">
      {isLoading ? <Loading /> : null}
      <div className="max-w-sm w-3/4 min-h-min px-10 mx-auto my-10 self-center pb-10 pt-6 bg-slate-500 bg-opacity-90 rounded-2xl shadow-xl">
        <h1 className="text-center text-2xl font-bold pb-5">
          <Link to="/">
            <IoIosArrowBack />
          </Link>
          {textHeader}
        </h1>
        <form
          className="flex flex-col gap-5"
          onSubmit={handlerSubmit}
          action=""
          method="post"
        >
          <input
            required
            onChange={handlerChange}
            type="text"
            value={input.name}
            name="name"
            placeholder="name"
            className="py-2 rounded"
          />
          <input
            required
            onChange={handlerChange}
            type="email"
            value={input.email}
            name="email"
            placeholder="email"
            className="py-2 rounded"
          />
          <input
            required
            onChange={handlerChange}
            type="password"
            value={input.password}
            name="password"
            placeholder="password"
            className="py-2 rounded"
          />
          <button
            className="btn btn-primary"
            onChange={handlerChange}
            type="submit"
          >
            Submit
          </button>
        </form>
      </div>
    </div>
  );
}

export default Register;
