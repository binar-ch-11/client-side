import React from 'react';
import {
  render, screen,
} from '@testing-library/react';
import '@testing-library/jest-dom';
import Register from './index';

jest.mock('axios');
jest.mock('react-router-dom');

describe('Register', () => {
  it('Should be Register', () => {
    render(<Register />);

    expect(screen.getByText('Register')).toBeInTheDocument();
    expect(screen.getByPlaceholderText('password')).toBeInTheDocument();
    expect(screen.getByText('Submit')).toBeInTheDocument();
  });

  it('error handling', async () => {
    render(<Register />);

    expect(screen.queryByText('email already exist!')).toBeNull();
  });
});
