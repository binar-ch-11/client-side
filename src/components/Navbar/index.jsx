import React from 'react';
import { Link } from 'react-router-dom';

function Navbar() {
  return (
    <div className="f fixed top-0 left-0 right-0 bg-black shadow-lg text-white shadow-white flex justify-end px-5 py-3 z-30">
      <div className="flex gap-5">
        <Link to="/profile">Profile</Link>
        <Link to="/chat">Room Chat</Link>
        <Link to="/pdf">Create PDF</Link>
      </div>
    </div>
  );
}

export default Navbar;
