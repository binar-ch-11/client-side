import React from 'react';
import { CirclesWithBar, ThreeCircles } from 'react-loader-spinner';

function Loading() {
  return (
    <div
      style={{ backgroundColor: 'rgba(100,100,100,0.7)' }}
      className="absolute top-0 bottom-0 right-0 left-0 flex justify-center"
    >
      <div className="self-center flex gap-10">
        <ThreeCircles
          height="100"
          width="100"
          color="#4fa94d"
          wrapperStyle={{}}
          wrapperClass=""
          visible
          ariaLabel="three-circles-rotating"
          outerCircleColor="#84cc16"
          innerCircleColor="#facc15"
          middleCircleColor="#dc2626"
        />
        <CirclesWithBar
          height="100"
          width="100"
          color="#4fa94d"
          wrapperStyle={{}}
          wrapperClass=""
          visible
          outerCircleColor="#84cc16"
          innerCircleColor="#facc15"
          barColor="#dc2626"
          ariaLabel="circles-with-bar-loading"
        />
        <ThreeCircles
          height="100"
          width="100"
          color="#4fa94d"
          wrapperStyle={{}}
          wrapperClass=""
          visible
          ariaLabel="three-circles-rotating"
          outerCircleColor="#84cc16"
          innerCircleColor="#facc15"
          middleCircleColor="#dc2626"
        />
      </div>
    </div>
  );
}

export default Loading;
