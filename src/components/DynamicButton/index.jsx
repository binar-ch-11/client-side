import React from 'react';
import { ProgressBar } from 'react-loader-spinner';
import PropTypes from 'prop-types';

function DynamicButton({ dynamicButton, text }) {
  return (
    <button
      type="submit"
      disabled={dynamicButton === true}
      className="bg-green-500 px-3 py-2 rounded-lg hover:bg-green-600 text-white disabled:hover:cursor-not-allowed w-20 h-10 flex justify-center"
    >
      {dynamicButton ? (
        <span className="self-center">
          <ProgressBar
            height="40"
            width="40"
            ariaLabel="progress-bar-loading"
            wrapperStyle={{}}
            wrapperClass="progress-bar-wrapper"
            borderColor="#F4442E"
            barColor="#51E5FF"
          />
        </span>
      ) : (
        { text }
      )}
    </button>
  );
}

DynamicButton.propTypes = {
  dynamicButton: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
};

export default DynamicButton;
