import React from 'react';
import './App.css';
import Router from './route';

function App() {
  return <Router />;
}

export default App;
